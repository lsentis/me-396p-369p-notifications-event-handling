import abc

# common interface to implement subscibe and notify notifiees
class Notifiee:
	# statement to specify abstract methods
	__metaclass__ = abc.ABCMeta

	# a suggested constructor for registering notifiees
	def __init__(self, notifier, notifiee):
		notifier.setNotifiee( notifiee )

	# an abstract general notification method that must be
	# implemented by the notifiee
	@abc.abstractmethod
	def onEvent(self):
		pass

# an example of a notifier
class Account:
	# constructor initializes lists of notifiers
	def __init__(self, balance=0):
		self.__balance = balance
		self.__balanceNotifiee = [] # empty list
		self.__zeroNotifiee = [] # empty list

	def getBalance(self):
		return self.__balance

	# account notifies on balance changes and on zero balance
	def setBalance(self, value):
		self.__balance = value
		# loop to notify all notifiees subscribed to the list
		for item in self.__balanceNotifiee:
			item.onEvent()

		# loop to notify all notifiees subscribed to the list
		if self.__balance == 0:
			for item in self.__zeroNotifiee:
				item.onEvent()			

	# method to register notifiees to the notifier account
	def setBalanceNotifiee(self, notifiee):
		self.__balanceNotifiee.append( notifiee )

	# method to register notifiees to the notifier account
	def setZeroNotifiee(self, notifiee):
		self.__zeroNotifiee.append( notifiee )

class DoomsdayClock:
	# nested notifiee
	class ZeroTimeNotifiee( Notifiee ):
		pass

	def __init__(self, timeLeft=7):
		self.__timeLeft = timeLeft
		self.__zeroTimeNotifiee = []

	# the clock notifies on zero time left
	def setTimeLeft(self, value):
		self.__timeLeft = value
		# loop to notify all notifiees subscribed to the list
		if self.__timeLeft == 0:
			for item in self.__zeroTimeNotifiee:
				item.onEvent()			

	# method to register notifiees to the notifier clock
	def setZeroTimeNotifiee(self, notifiee):
		self.__zeroTimeNotifiee.append( notifiee )

class GeneralReactor():
	# nested reactor class implements all notifiees
	class BalanceAccountNotifiee( Notifiee ): # interface for multiple subscriptions
		def __init__(self, account, reactor):
			account.setBalanceNotifiee( self ) # register notifiee
			self.__reactor = reactor
		def onEvent(self):
			self.__reactor.onBalance()

	# nested reactor class implements all notifiees
	class ZeroAccountNotifiee( Notifiee ): # interface for multiple subscriptions
		def __init__(self, account, reactor):
			account.setZeroNotifiee( self ) # register notifiee
			self.__reactor = reactor
		def onEvent(self):
			self.__reactor.onZeroBalance()

	# nested reactor class implements all notifiees
	# notifiees can be subclassed from 
	class ZeroTimeNotifiee( Notifiee ):
		def __init__(self, clock, reactor):
			clock.setZeroTimeNotifiee( self ) # register notifiee
			self.__reactor = reactor
		def onEvent(self):
			self.__reactor.onZeroTime()

	def __init__(self, account, clock):
		GeneralReactor.BalanceAccountNotifiee(account, self)
		GeneralReactor.ZeroAccountNotifiee(account, self)
		GeneralReactor.ZeroTimeNotifiee(clock, self)

	def onBalance(self):
		print 'There has been a change in balance'

	def onZeroBalance(self):
		print 'The account is empty'

	def onZeroTime(self):
		print 'It\'s time to transcend'

# create notifier
acc = Account(0)
clock = DoomsdayClock()

# define reactor class
react = GeneralReactor(acc, clock) # notifiee

# make changes
acc.setBalance(10)
acc.setBalance(20)
acc.setBalance(0)

clock.setTimeLeft(5)
clock.setTimeLeft(0)


