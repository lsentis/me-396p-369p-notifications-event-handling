class Account:
	# nested class
	class Notifiee:
		def onBalance(self):
			print 'this is the base class'

	def __init__(self, balance=0):
		self.__balance = balance
		self.__notifiee = [] # empty list

	def getBalance(self):
		return self.__balance

	def setBalance(self, value):
		self.__balance = value
		for item in self.__notifiee:
			item.onBalance()

	def setNotifiee(self, notifiee):
		self.__notifiee.append( notifiee )

# reactor class implements notifiee
class AccountReactor( Account.Notifiee ):
	def __init__(self, account):
		account.setNotifiee(self)
	# def onBalance(self):
	#  	print 'The balance has changed'

class AnotherReactor( Account.Notifiee ):
	def __init__(self, account):
		account.setNotifiee(self)
	def onBalance(self):
		print 'This is the other reactor'

# create notifier
acc = Account(0)

# define reactor class
react = AccountReactor(acc) # notifiee
react2 = AccountReactor(acc)
# another reactor
react3 = AnotherReactor(acc)

# make changes
acc.setBalance(10)
acc.setBalance(20)

