class Notifiee:
	def __init__(self, notifier, notifiee):
		self.__notifier = notifier
		self.__notifier.setNotifiee( notifiee )

	def onEvent(self):
		print 'default notifiee'

class Account:
	# nested class
	class BalanceNotifiee( Notifiee ):
		pass # using parent's constructor

	def __init__(self, balance=0):
		self.__balance = balance
		self.__notifiee = [] # empty list

	def getBalance(self):
		return self.__balance

	def setBalance(self, value):
		self.__balance = value
		for item in self.__notifiee:
			item.onEvent()

	def setNotifiee(self, notifiee):
		self.__notifiee.append( notifiee )

# reactor class implements notifiee
class AccountReactor():
	def __init__(self, account):
		account.BalanceNotifiee( account, self ) # constructor without assignment
	def onEvent(self):
		print 'This is the standard reactor'

class AnotherReactor():
	def __init__(self, account):
		account.BalanceNotifiee( account, self )
	def onEvent(self):
		print 'This is the other reactor'

# create notifier
acc = Account(0)

# define reactor class
react = AccountReactor(acc) # notifiee
react2 = AccountReactor(acc)
# another reactor
react3 = AnotherReactor(acc)

# make changes
acc.setBalance(10)
acc.setBalance(20)