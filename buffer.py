class Notifiee:
	def __init__(self, notifier, notifiee):
		self.__notifier = notifier
		self.__notifier.setNotifiee( notifiee )

	def onEvent(self):
		print 'default notifiee'

class Buffer:
	class FullNotifiee( Notifiee ):
		pass # using parent's constructor

	class EmptyNotifiee( Notifiee ):
		pass

	class ActiveNotifiee( Notifiee ):
		pass

	def __init__(self):
		self.__queue = []
		self.__notifiee = []

	def write(self, value):
		self.__queue.append( value )
		if len( self.__queue ) == 10:
			for item in self.__notifiee:
				item.onEvent()

	def read( self ):
		last = self.__queue[-1] # access last element
		self.__queue.pop() # remove last element
		return last

	def setNotifiee(self, notifiee):
		self.__notifiee.append( notifiee )

# producer class
class Producer:
	def __init__(self, buffer):
		self.__buffer = buffer
		buffer.FullNotifiee( buffer, self ) # constructor without assignment

	def onEvent(self):
		self.__pause()
		print 'paused'

	def write(self):
		self.__buffer.write(1)

	def __pause(self):
		pass # should be implemented

# main
buff = Buffer()
prod = Producer( buff )

for ii in range(0,11):
	prod.write()